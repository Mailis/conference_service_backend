package ee.tallink.conference_service_test;

import static io.restassured.RestAssured.delete;
import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertTrue;

//import org.junit.Before;
import org.junit.jupiter.api.Test;

import ee.tallink.conference_service_test.models.Conference;
import ee.tallink.conference_service_test.models.ConferenceRoom;

public class ConferenceServiceTest {
	String roomName = "Cloudy";
	String roomLocation = "SKY CONFERENCE";
	int maxSeats = 4;

	String confName = "REST-TEST-VEST-LUST";
	long confStart = -143875401000L;
	long confEnd = -112339401000L;
	boolean confCancelled = false;

	@Test
	public void createConferenceRoomTest() {
		ConferenceRoom room4 = new ConferenceRoom(roomName, roomLocation, maxSeats);
		
		given()
		.contentType("application/json")
		.body(room4)
		.when()
		.post("/room")
		.then()
		.statusCode(200);
	}

	@Test
	public void readConferenceRoomTest() {
		get("/room/1").then().statusCode(200);
	}

	@Test
	public void createConferenceTest() {
		Conference conference = new Conference(confName, confStart, confEnd, null, confCancelled);

		given()
		.contentType("application/json")
		.body(conference)
		.when()
		.post("/conference")
		.then()
		.statusCode(200);
	}

	@Test
	public void readConferenceDataTest() {
		get("/conference/1").then().assertThat().statusCode(200);
	}

	@Test
	public void getConferenceAvailabiltyTest() {
		get("/conference/1/availability")
			.then()
			.statusCode(200);
	}



}

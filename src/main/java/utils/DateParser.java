package utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateParser {

	public static Date parseDate(String dateString){
		try {
		    SimpleDateFormat format =
		        new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
		    return format.parse(dateString);
		}
		catch(ParseException pe) {
		    throw new IllegalArgumentException(pe);
		}
	}
}

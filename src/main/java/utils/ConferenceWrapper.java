package utils;
import ee.tallink.conference_service_test.models.Conference;

public class ConferenceWrapper extends Conference{

	int availabilty;
	int max_seats;
	int fillment;
	
	public ConferenceWrapper(int availabilty, int max_seats, int fillment) {
		this.availabilty = availabilty;
		this.max_seats = max_seats;
		this.fillment = fillment;
	}
	
	public ConferenceWrapper(int availabilty, int max_seats) {
		this.availabilty = availabilty;
		this.max_seats = max_seats;
	}
	
	public ConferenceWrapper(int availabilty) {
		this.availabilty = availabilty;
	}
}

package ee.tallink.conference_service_test.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ee.tallink.conference_service_test.models.ConferenceRoom;

@Repository
public interface ConferenceRoomRepository extends JpaRepository<ConferenceRoom, Long>{
	List<ConferenceRoom> findAllByOrderByName();
	
	//get conference room
    @Query(value="SELECT * FROM conference_room  cr " + 
    		"WHERE cr.id IN "
    		+ "(SELECT conference_room_id from conference c WHERE c.id = ?1)", nativeQuery = true)
    List<ConferenceRoom> findRoomByConferenceId(long confId);
}

package ee.tallink.conference_service_test.repositories;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ee.tallink.conference_service_test.models.Conference;

@Repository
public interface ConferenceRepository extends JpaRepository<Conference, Long>{
	List<Conference> findByNameContainingIgnoreCase(String name);
	List<Conference> findByStartDate(long date);
    List<Conference> findByConferenceRoomId(long conferenceRoomId, Sort sort);
	List<Conference> findAllByOrderByStartDateAscName();
	
	@Query(value="SELECT id FROM conference Where conference.is_cancelled = false", nativeQuery = true)
	List<Long> getAllConferenceIds();
    
    //get number of max seats
    @Query(value="SELECT max_seats FROM conference_room  cr " + 
    		"WHERE cr.id = (SELECT conference_room_id from conference c where c.id = ?1)", nativeQuery = true)
    int getConferenceRoomMaxSeats(long id);
    
    //get number of taken seats
    @Query(value="SELECT count(*) FROM participant_conference pc "
    				+ "WHERE pc.conference_id = ?1", nativeQuery = true)
    int getConferenceNumberOfParticipants(long id);
    
  //get number of taken seats
    @Query(value="DELETE FROM participant_conference pc "
			+ "WHERE pc.participant_id = ?1 AND pc.conference_id = ?2", nativeQuery = true)
	void removeParticipantFromConference(long participantId, long confId);
	    

    //get participant's conferences
    @Query(value="SELECT * FROM conference c "
        	+ "WHERE c.id IN (SELECT pc.conference_id FROM participant_conference pc WHERE pc.participant_id = ?1)", nativeQuery = true)  	
	List<Conference> findParticipantsConferences(long participantId);
}

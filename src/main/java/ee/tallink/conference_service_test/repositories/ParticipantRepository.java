package ee.tallink.conference_service_test.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ee.tallink.conference_service_test.models.Participant;

@Repository
public interface ParticipantRepository extends JpaRepository<Participant, Long>{
	List<Participant> findByFullNameContainingIgnoreCase(String fullName);
	List<Participant> findAllByOrderByFullNameAsc();	
	
	//get conference participants
    @Query(value="SELECT * FROM participant p "
    	+ "WHERE p.id IN (SELECT pc.participant_id FROM participant_conference pc WHERE pc.conference_id = ?1)", nativeQuery = true)
	List<Participant> findParticipantsByConferenceId(long confId);

    @Transactional
    @Modifying
    @Query(value="INSERT INTO participant_conference  (participant_id, conference_id) " + 
    		     "SELECT ?1, c.id FROM conference c WHERE c.is_cancelled = false AND c.id = ?2", nativeQuery = true)
    void addParticipantToConference(Long pId, Long confId);

    @Transactional
    @Modifying
    @Query(value="DELETE FROM participant_conference "
    		+ "WHERE participant_id = ?1 AND  conference_id = ?2 ", nativeQuery = true)
    void removeParticipantFromConference(Long pId, Long confId);

}

package ee.tallink.conference_service_test.models;

import java.util.Set;

import javax.persistence.*;

import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class ConferenceRoom extends RepresentationModel<ConferenceRoom> {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

	@Column(length = 100, nullable = false)
    private String name;
	
	@Column(length = 100, nullable = false)
    private String location;

    private int maxSeats;
    
    @JsonIgnore
    @OneToMany(mappedBy = "conferenceRoom", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<Conference> conferences;
    
    public ConferenceRoom(String name, String location, int maxSeats){
    	this.name = name;
    	this.location = location;
    	this.maxSeats = maxSeats;
    }
    public ConferenceRoom() {}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getMaxSeats() {
		return maxSeats;
	}

	public void setMaxSeats(int maxSeats) {
		this.maxSeats = maxSeats;
	}
	
	public Set<Conference> getConferences() {
		return conferences;
	}
	public void setConferences(Set<Conference> conferences) {
		this.conferences = conferences;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
}

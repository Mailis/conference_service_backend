package ee.tallink.conference_service_test.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import org.springframework.hateoas.RepresentationModel;


@Entity
public class Conference extends RepresentationModel<Conference> {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(length = 150, nullable = false)
    private String name;

	@Column(nullable = false)
    private long startDate;
	@Column(nullable = false)
    private long endDate;
    
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinTable(name = "participant_conference",
            joinColumns = {
            		@JoinColumn(name = "conference_id", referencedColumnName = "id",
                            nullable = false, updatable = false)},
            inverseJoinColumns = {
                    @JoinColumn(name = "participant_id", referencedColumnName = "id",
                            nullable = false, updatable = false)})
    private List<Participant> participants = new ArrayList<>();
    
    @ManyToOne
    private ConferenceRoom conferenceRoom;
    
    @Column(columnDefinition = "boolean default false")
    private boolean isCancelled;
    
    public Conference(){}

    public Conference(String name, long startDate, long endDate){
    	this.setName(name);
    	this.setStartDate(startDate);
    	this.setEndDate(endDate);
    }

    public Conference(String name, long startDate, long endDate, ConferenceRoom conferenceRoom){
    	this.setName(name);
    	this.setStartDate(startDate);
    	this.setEndDate(endDate);
    	this.setConferenceRoom(conferenceRoom);
    }
    public Conference(String name, long startDate, long endDate, ConferenceRoom conferenceRoom, boolean isCancelled){
    	this.setName(name);
    	this.setStartDate(startDate);
    	this.setEndDate(endDate);
    	this.setConferenceRoom(conferenceRoom);
    	this.setIsCancelled(isCancelled);
    }
    
    public String getName() {
		return name;
	}
    
	public void setName(String name) {
		this.name = name;
	}

	public long getStartDate() {
		return startDate;
	}
	
	public void setStartDate(long startDate) {
		this.startDate = startDate;
	}
	public long getEndDate() {
		return endDate;
	}
	
	public void setEndDate(long endDate) {
		this.endDate = endDate;
	}

	public ConferenceRoom getConferenceRoom() {
		return conferenceRoom;
	}

	public void setConferenceRoom(ConferenceRoom conferenceRoom) {
		this.conferenceRoom = conferenceRoom;
	}

	public List<Participant> getParticipants() {
		return participants;
	}
	
	public void setParticipants(List<Participant> p) {
		this.participants = p;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean getIsCancelled() {
		return isCancelled;
	}

	public void setIsCancelled(boolean isCancelled) {
		this.isCancelled = isCancelled;
	}
}

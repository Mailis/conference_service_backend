package ee.tallink.conference_service_test.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OrderBy;

import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Participant extends RepresentationModel<Participant> {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(nullable = false)
	@OrderBy("value ASC")
    private String fullName;
	
    private long birthDate;
    

    @ManyToMany(mappedBy = "participants", fetch = FetchType.LAZY)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<Conference> conferences = new ArrayList<Conference>();
    
    public Participant() {}
    
    public Participant(String fullName, long birthDate){
    	this.fullName = fullName;
    	this.birthDate = birthDate;
    }
    
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	public String getFullName() {
		return this.fullName;
	}
	
	public void setBirthDate(long birthDate) {
		this.birthDate = birthDate;
	}
	
	public long getBirthDate() {
		return this.birthDate;
	}

	public List<Conference> getConferences() {
		return conferences;
	}

	public void setConferences(List<Conference> conferences) {
		this.conferences = conferences;
	}


	public void addToConference(Conference conference) {
		this.conferences.add(conference);
	}

	public void addToConferences(List<Conference> conferences) {
		for(Conference c : conferences) {
			this.addToConference(c);
		}
	}
	
	public void removeFromConference(Conference conference) {
		this.conferences.remove(conference);
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}

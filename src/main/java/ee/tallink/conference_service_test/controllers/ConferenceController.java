package ee.tallink.conference_service_test.controllers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ee.tallink.conference_service_test.models.Conference;
import ee.tallink.conference_service_test.services.ConferenceService;
import ee.tallink.conference_service_test.services.ParticipantService;

@RestController
@RequestMapping(value = "/conference", produces = { "application/json", "application/hal+json" })
public class ConferenceController {

	@Autowired
	ConferenceService conferenceService;
	@Autowired
	ParticipantService participantService;
	
	public ConferenceController() {}

	@GetMapping
	public HttpEntity<CollectionModel<Conference>> index() {
		List<Conference> conferences =  conferenceService.getAll();
		for (final Conference conference : conferences) {
			conferenceService.addLinks(conference);
	    }
	    Link collectionLink = linkTo(methodOn(ConferenceController.class).index()).withSelfRel();
	    Link availLink = linkTo(methodOn(ConferenceController.class).getAllAvailabilities()).withRel("all_availabilities");
	    Link fillLink = linkTo(methodOn(ConferenceController.class).getMultipleRoomFillment(null)).withRel("multi_fillment");
	    Link capacityLink = linkTo(methodOn(ConferenceController.class).getMultipleRoomMaxSeats(null)).withRel("multi_capacity");
	    Link[] links = new Link[] {collectionLink, availLink, fillLink, capacityLink};
		return new ResponseEntity<>(CollectionModel.of(conferences, links), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public HttpEntity<Conference> getConferenceById(@PathVariable String id) {
		Conference conference = conferenceService.getOneById(id);
		conferenceService.addLinks(conference);
		return new ResponseEntity<>(conference, HttpStatus.OK);
	}
	
	@GetMapping("/{id}/room-capacity")
	public HttpEntity<Integer> getConfRoomCapacity(@PathVariable String id) {
		return new ResponseEntity<>(conferenceService.getConferenceRoomMaxSeats(id), HttpStatus.OK);
	}

	@GetMapping("/{id}/availability")
	public HttpEntity<Integer> getConfRoomAvailability(@PathVariable String id) {
		return new ResponseEntity<>(conferenceService.getAvailability(id), HttpStatus.OK);
	}

	@GetMapping("/{id}/fillment")
	public HttpEntity<Integer> getConfRoomFillment(@PathVariable String id) {
		return new ResponseEntity<>(conferenceService.getConferenceRoomFillment(id), HttpStatus.OK);
	}

	@GetMapping("/all_availabilities")
	public HttpEntity<Map<Long, Integer>> getAllAvailabilities() {
		return new ResponseEntity<>(conferenceService.getAllAvailabilities(), HttpStatus.OK);
	}
	
	
	@PostMapping("/availability")
	public HttpEntity<Map<Long, Integer>> getMultipleAvailability(@RequestBody String[] ids) {
		return new ResponseEntity<>(conferenceService.getMultipleAvailability(ids), HttpStatus.OK);
	}
	@PostMapping("/fillment")
	public HttpEntity<Map<Long, Integer>> getMultipleRoomFillment(@RequestBody String[] ids) {
		return new ResponseEntity<>(conferenceService.getMultipleRoomFillment(ids), HttpStatus.OK);
	}
	@PostMapping("/room-capacity")
	public HttpEntity<Map<Long, Integer>> getMultipleRoomMaxSeats(@RequestBody String[] ids) {
		return new ResponseEntity<>(conferenceService.getMultipleRoomMaxSeats(ids), HttpStatus.OK);
	}
	
	
	@PostMapping(consumes = "application/json", produces = {"application/hal+json" })
	public HttpEntity<Conference> create(@RequestBody Conference conference) {
		Conference savedConf =  conferenceService.save(conference);
		Conference integralConf =  conferenceService.getOneById(savedConf.getId().toString());
		conferenceService.addLinks(integralConf);
		return new ResponseEntity<>(integralConf, HttpStatus.OK);
	}

	@PutMapping(consumes = "application/json")
	public HttpEntity<Conference> update(@RequestBody Conference conference) {
		Conference upd = conferenceService.update(conference);
		return new ResponseEntity<>(upd, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public HttpEntity<Boolean> delete(@PathVariable String id) {
		boolean b = conferenceService.delete(id);
		HttpStatus status = b? HttpStatus.OK : HttpStatus.NOT_IMPLEMENTED;
		return new ResponseEntity<>(b, status);
	}
}

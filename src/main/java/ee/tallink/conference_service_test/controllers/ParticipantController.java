package ee.tallink.conference_service_test.controllers;


import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ee.tallink.conference_service_test.models.Conference;
import ee.tallink.conference_service_test.models.Participant;
import ee.tallink.conference_service_test.services.ParticipantService;

@RestController
@RequestMapping(value="/participant", produces = { "application/json", "application/hal+json" })
public class ParticipantController {
	@Autowired
	ParticipantService participantService;
	
	@GetMapping()
	public HttpEntity<CollectionModel<Participant>> index() {
		List<Participant> participants =  participantService.getAll();
		for(Participant p : participants) {
			participantService.addLinks(p);
		}
		Link collectionLink = linkTo(methodOn(ParticipantController.class).index()).withSelfRel();
		return new ResponseEntity<>(CollectionModel.of(participants, collectionLink), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public HttpEntity<Participant> getParticipantById(@PathVariable String id) {
		Participant partic = participantService.getOneById(id);
		participantService.addLinks(partic);
		return new ResponseEntity<>(partic, HttpStatus.OK);
	}

	@GetMapping("/conference/{id}")
	public HttpEntity<CollectionModel<Participant>> getParticipantsByConferenceId(@PathVariable String id) {
		List<Participant> participants = participantService.findParticipantsByConferenceId(id);
		Link collectionLink = linkTo(methodOn(ParticipantController.class).index()).withSelfRel();
		return new ResponseEntity<>(CollectionModel.of(participants, collectionLink), HttpStatus.OK);
	}
	
	@GetMapping("/{id}/conference")
	public HttpEntity<CollectionModel<Conference>> getParticipantConferences(@PathVariable String id) {
		List<Conference> participantConferences = participantService.getParticipantConferences(id);
		Link collectionLink = linkTo(methodOn(ConferenceController.class).index()).withSelfRel();
		return new ResponseEntity<>(CollectionModel.of(participantConferences, collectionLink), HttpStatus.OK);
	}

	@PostMapping(consumes = "application/json")
	public Participant create(@RequestBody Participant participant){
		return participantService.save(participant);
	}

	@PutMapping(consumes = "application/json")
	public Participant update(@RequestBody Participant participant) {
		return participantService.update(participant);
	}
	
	@PostMapping(value="/{id}/add-to-conference")
	public HttpEntity<Map<String, Long>> addParticipantToConference(@PathVariable String id, @RequestBody String  conferenceId) {
		Map<String, Long> p = participantService.addParticipantToConference(id, conferenceId);
		if(p == null) {//not available seats in a conference
			//410 Gone
			return new ResponseEntity<>(p, HttpStatus.GONE);
		}
		else {
			return new ResponseEntity<>(p, HttpStatus.OK);
		}
	}
	
	@DeleteMapping(value="/{id}/remove-from-conference")
	public HttpEntity<Map<String, Long>> removeParticipantFromConference(@PathVariable String id, @RequestBody String  conferenceId) {
		Map<String, Long> p = participantService.removeParticipantFromConference(id, conferenceId);
		return new ResponseEntity<>(p, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public HttpEntity<Boolean> deleteParticipant(@PathVariable String id) {
		boolean b = participantService.deleteParticipant(id);
		HttpStatus status = b? HttpStatus.OK : HttpStatus.NOT_IMPLEMENTED;
		return new ResponseEntity<>(b, status);
	}
}

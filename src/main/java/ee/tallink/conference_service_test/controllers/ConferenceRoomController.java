package ee.tallink.conference_service_test.controllers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ee.tallink.conference_service_test.models.ConferenceRoom;
import ee.tallink.conference_service_test.services.ConferenceRoomService;

@RestController
@RequestMapping(value="/room", produces = { "application/json", "application/hal+json" })
public class ConferenceRoomController {
	@Autowired
	ConferenceRoomService conferenceRoomService;
	
	@GetMapping()
	public HttpEntity<CollectionModel<ConferenceRoom>> index() {
		List<ConferenceRoom> confRooms =  conferenceRoomService.getAll();
		for (final ConferenceRoom room : confRooms) {
			conferenceRoomService.addLinks(room);
	    }
		Link collectionLink = linkTo(methodOn(ConferenceRoomController.class).index()).withSelfRel();
		return new ResponseEntity<>(CollectionModel.of(confRooms, collectionLink), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public HttpEntity<ConferenceRoom> getConferenceRoomById(@PathVariable String id) {
		ConferenceRoom room = conferenceRoomService.getOneById(id);
		conferenceRoomService.addLinks(room);
		return new ResponseEntity<>(room, HttpStatus.OK);
	}

	@GetMapping("/conference/{id}")
	public HttpEntity<ConferenceRoom> getConferenceRoomByConferenceId(@PathVariable String id) {
		ConferenceRoom confRoom =  conferenceRoomService.findRoomByConferenceId(id);
		if(confRoom != null) {
			conferenceRoomService.addLinks(confRoom);
			return new ResponseEntity<>(confRoom, HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping
	public ConferenceRoom create(@RequestBody ConferenceRoom room){
		return conferenceRoomService.save(room);
	}

	@PutMapping(consumes = "application/json")
	public ConferenceRoom update(@RequestBody ConferenceRoom room) {
		return conferenceRoomService.update(room);
	}

	@DeleteMapping("/{id}")
	public HttpEntity<Boolean> delete(@PathVariable String id) {
		boolean b = conferenceRoomService.delete(id);
		HttpStatus status = b? HttpStatus.OK : HttpStatus.NOT_IMPLEMENTED;
		return new ResponseEntity<>(b, status);
	}
}

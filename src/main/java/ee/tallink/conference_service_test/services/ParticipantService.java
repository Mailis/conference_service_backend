package ee.tallink.conference_service_test.services;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ee.tallink.conference_service_test.controllers.ParticipantController;
import ee.tallink.conference_service_test.models.Conference;
import ee.tallink.conference_service_test.models.Participant;
import ee.tallink.conference_service_test.repositories.ConferenceRepository;
import ee.tallink.conference_service_test.repositories.ParticipantRepository;

@Service
public class ParticipantService {
	@Autowired
	ParticipantRepository participantRepository;
	@Autowired
	ConferenceRepository conferenceRepository;
	@Autowired
	ConferenceService conferenceService;

	public void addLinks(Participant p) {
		String id = p.getId().toString();
		p.add(linkTo(methodOn(ParticipantController.class).getParticipantById(id)).withSelfRel());
		p.add(linkTo(methodOn(ParticipantController.class).create(p)).withRel("create"));
		p.add(linkTo(methodOn(ParticipantController.class).update(p)).withRel("update"));
		p.add(linkTo(methodOn(ParticipantController.class).deleteParticipant(id)).withRel("delete"));
		p.add(linkTo(methodOn(ParticipantController.class).getParticipantConferences(id)).withRel("participant_conferences"));
		p.add(linkTo(methodOn(ParticipantController.class).addParticipantToConference(id, id)).withRel("addToConference"));
		p.add(linkTo(methodOn(ParticipantController.class).removeParticipantFromConference(id, id)).withRel("removeFromConference"));
	}

	public List<Participant> getAll() {
		return participantRepository.findAllByOrderByFullNameAsc();
	}

	public Participant getOneById(String id) {
		Long participantId = Long.parseLong(id);
		return participantRepository.findById(participantId).get();
	}
	

	public List<Conference> getParticipantConferences(String id) {
		Long participantId = Long.parseLong(id);
		return conferenceRepository.findParticipantsConferences(participantId);
	}	 

	public List<Participant> findParticipantsByConferenceId(String id) {
		Long confId = Long.parseLong(id);
		return participantRepository.findParticipantsByConferenceId(confId);
	}	 

	public Participant save(Participant participant){
		return participantRepository.save(participant);
	}

	public Participant update(Participant participant) {
		Participant participantToUpdate = participantRepository.getOne(participant.getId());
		participantToUpdate.setFullName(participant.getFullName());
		participantToUpdate.setBirthDate(participant.getBirthDate());
		return participantRepository.save(participantToUpdate);
	}

	public boolean deleteParticipant(String id) {
		Long participantId = Long.parseLong(id);
		if(participantRepository.existsById(participantId)){
			participantRepository.deleteById(participantId);
			return true;
		}
		return false;
	}

	public Map<String, Long> addParticipantToConference(String participantId, String  conferenceId) {
		Long pId = Long.parseLong(participantId);
		Long confId = Long.parseLong(conferenceId);
		int availability = conferenceService.getAvailability(conferenceId);
		if(availability > 0) {
			participantRepository.addParticipantToConference(pId, confId);
			Map<String, Long> result = new HashMap<String, Long>();
			result.put("participantId", pId);
			result.put("conferenceId", confId);
			return result;
		}
		return null;
	}

	public Map<String, Long> removeParticipantFromConference(String participantId, String  conferenceId) {
		Long pId = Long.parseLong(participantId);
		Long confId = Long.parseLong(conferenceId);
		participantRepository.removeParticipantFromConference(pId, confId);
		Map<String, Long> result = new HashMap<String, Long>();
		result.put("participantId", pId);
		result.put("conferenceId", confId);
		return result;
	}
}

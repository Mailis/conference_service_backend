package ee.tallink.conference_service_test.services;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Service;

import ee.tallink.conference_service_test.controllers.ConferenceController;
import ee.tallink.conference_service_test.controllers.ConferenceRoomController;
import ee.tallink.conference_service_test.controllers.ParticipantController;
import ee.tallink.conference_service_test.models.Conference;
import ee.tallink.conference_service_test.models.ConferenceRoom;
import ee.tallink.conference_service_test.models.Participant;
import ee.tallink.conference_service_test.repositories.ConferenceRepository;

@Service
public class ConferenceService {
	@Autowired
	ConferenceRepository conferenceRepository;
	@Autowired
	ParticipantService participantService;

	public void addLinks(Conference conference) {
		String confID = conference.getId().toString();
		// add self link to conference
		conference.add(linkTo(methodOn(ConferenceController.class).getConferenceById(confID)).withSelfRel());

		conference.add(linkTo(methodOn(ConferenceController.class).create(conference)).withRel("create"));
		conference.add(linkTo(methodOn(ConferenceController.class).update(conference)).withRel("update"));
		conference.add(linkTo(methodOn(ConferenceController.class).delete(confID)).withRel("delete"));
		// availability
		conference.add(linkTo(methodOn(ConferenceController.class)
                .getConfRoomAvailability(confID)).withRel("room_availability"));
		//room fillment 
		conference.add(linkTo(methodOn(ConferenceController.class)
                .getConfRoomFillment(confID)).withRel("room_fillment"));
		//room max seats 
		conference.add(linkTo(methodOn(ConferenceController.class)
                .getConfRoomCapacity(confID)).withRel("room_capacity"));

		// add link to conference participants
		conference.add(linkTo(methodOn(ParticipantController.class).getParticipantsByConferenceId(confID))
				.withRel("participants").expand(confID));
		// add link to conference room
		conference.add(linkTo(methodOn(ConferenceRoomController.class).getConferenceRoomByConferenceId(confID))
				.withRel("room"));

		// add self link to room
		ConferenceRoom room = conference.getConferenceRoom();
		if (room != null) {
			// avoid duplicate links on same thing
			Link selfLink = linkTo(methodOn(ConferenceRoomController.class).getConferenceRoomById(room.getId().toString())).withSelfRel();
			if (room.getLink(selfLink.getRel()).isEmpty()) {
				room.add(selfLink);
			}
		}

		// add selflink to each participant
		List<Participant> participants = participantService.findParticipantsByConferenceId(confID);
		for (final Participant participant : participants) {
			Link selfLink = linkTo(
					methodOn(ParticipantController.class).getParticipantById(participant.getId().toString()))
							.withSelfRel();
			// avoid duplicate links on same thing
			if (participant.getLink(selfLink.getRel()).isEmpty()) {
				participant.add(selfLink);
			}
		}
	}

	public List<Conference> getAll() {
		return conferenceRepository.findAllByOrderByStartDateAscName();
	}

	public Conference getOneById(String id) {
		Long confId = Long.parseLong(id);
		return conferenceRepository.findById(confId).get();
	}

	public Conference save(Conference conference) {
		return conferenceRepository.save(conference);
	}

	public Conference update(Conference conference) {
		Conference conferenceToUpdate = conferenceRepository.findById(conference.getId()).get();
		conferenceToUpdate.setName(conference.getName());
		conferenceToUpdate.setIsCancelled(conference.getIsCancelled());
		conferenceToUpdate.setStartDate(conference.getStartDate());
		conferenceToUpdate.setEndDate(conference.getEndDate());
		return conferenceRepository.save(conferenceToUpdate);
	}

	public boolean delete(String id) {
		Long confId = Long.parseLong(id);
		if (conferenceRepository.existsById(confId)) {
			conferenceRepository.deleteById(confId);
			return true;
		}
		return false;
	}

	public int getConferenceRoomMaxSeats(String id) {
		Long confId = Long.parseLong(id);
		return conferenceRepository.getConferenceRoomMaxSeats(confId);
	}

	public int getConferenceRoomFillment(String id) {
		Long confId = Long.parseLong(id);
		return conferenceRepository.getConferenceNumberOfParticipants(confId);
	}

	public int getAvailability(String id) {
		Long confId = Long.parseLong(id);
		int nrOfParticipants = conferenceRepository.getConferenceNumberOfParticipants(confId);
		return conferenceRepository.getConferenceRoomMaxSeats(confId) - nrOfParticipants;
	}
	
	public Map<Long, Integer> getAllAvailabilities() {
		List<Long> confIDs = conferenceRepository.getAllConferenceIds();
		Map<Long, Integer> availabilities = new HashMap<Long, Integer>();
		for(Long id : confIDs) {
			int nrOfParticipants = conferenceRepository.getConferenceNumberOfParticipants(id);
			int avail = conferenceRepository.getConferenceRoomMaxSeats(id) - nrOfParticipants;
			availabilities.put(id, avail);
		}
		return availabilities;
	}

	public Map<Long, Integer> getMultipleAvailability(String[] ids) {
		Map<Long, Integer> availabilities = new HashMap<Long, Integer>();
		for(String id : ids) {
			Long confId = Long.parseLong(id);
			int nrOfParticipants = conferenceRepository.getConferenceNumberOfParticipants(confId);
			int avail = conferenceRepository.getConferenceRoomMaxSeats(confId) - nrOfParticipants;
			availabilities.put(confId, avail);
		}
		return availabilities;
	}

	public Map<Long, Integer> getMultipleRoomFillment(String[] ids) {
		Map<Long, Integer> fillments = new HashMap<Long, Integer>();
		for(String id : ids) {
			Long confId = Long.parseLong(id);
			int f= conferenceRepository.getConferenceNumberOfParticipants(confId);
			fillments.put(confId, f);
		}
		return fillments;
	}
	
	public Map<Long, Integer> getMultipleRoomMaxSeats(String[] ids) {
		Map<Long, Integer> capacities = new HashMap<Long, Integer>();
		for(String id : ids) {
			Long confId = Long.parseLong(id);
			int c = conferenceRepository.getConferenceRoomMaxSeats(confId);
			capacities.put(confId, c);
		}
		return capacities;
	}
}

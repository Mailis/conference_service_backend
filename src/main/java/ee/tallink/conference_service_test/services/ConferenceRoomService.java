package ee.tallink.conference_service_test.services;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ee.tallink.conference_service_test.controllers.ConferenceRoomController;
import ee.tallink.conference_service_test.models.ConferenceRoom;
import ee.tallink.conference_service_test.repositories.ConferenceRoomRepository;

@Service
public class ConferenceRoomService {
	@Autowired
	ConferenceRoomRepository conferenceRoomRepository;

	public void addLinks(ConferenceRoom room) {
		String id = room.getId().toString();
		room.add(linkTo(methodOn(ConferenceRoomController.class).getConferenceRoomById(id)).withSelfRel());
		room.add(linkTo(methodOn(ConferenceRoomController.class).create(room)).withRel("create"));
		room.add(linkTo(methodOn(ConferenceRoomController.class).update(room)).withRel("update"));
		room.add(linkTo(methodOn(ConferenceRoomController.class).delete(id)).withRel("delete"));
	}
	
	public List<ConferenceRoom> getAll() {
		return conferenceRoomRepository.findAllByOrderByName();
	}

	public ConferenceRoom getOneById(String id) {
		Long roomId = Long.parseLong(id);
		return conferenceRoomRepository.findById(roomId).get();
	}
	

	public ConferenceRoom findRoomByConferenceId(String id) {
		Long confId = Long.parseLong(id);
		List<ConferenceRoom> rooms = conferenceRoomRepository.findRoomByConferenceId(confId);
		Iterator<ConferenceRoom> it = rooms.iterator();
		if(it.hasNext()) {
			return it.next();
		}
		return null;
	}

	public ConferenceRoom save(ConferenceRoom room){
		return conferenceRoomRepository.save(room);
	}

	public ConferenceRoom update(ConferenceRoom room) {
		ConferenceRoom roomToUpdate = conferenceRoomRepository.getOne(room.getId());
		roomToUpdate.setName(room.getName());
		roomToUpdate.setLocation(room.getLocation());
		roomToUpdate.setMaxSeats(room.getMaxSeats());
		return conferenceRoomRepository.save(roomToUpdate);
	}

	public boolean delete(String id) {
		Long roomId = Long.parseLong(id);
		if(conferenceRoomRepository.existsById(roomId)){
			conferenceRoomRepository.deleteById(roomId);
			return true;
		}
		return true;
	}

}

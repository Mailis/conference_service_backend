package ee.tallink.conference_service_test;

import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import ee.tallink.conference_service_test.models.Conference;
import ee.tallink.conference_service_test.models.ConferenceRoom;
import ee.tallink.conference_service_test.models.Participant;
import ee.tallink.conference_service_test.repositories.ConferenceRepository;
import ee.tallink.conference_service_test.repositories.ConferenceRoomRepository;
import ee.tallink.conference_service_test.repositories.ParticipantRepository;

@SpringBootApplication
public class ConferenceServiceTestApplication{

	public static void main(String[] args) {
		SpringApplication.run(ConferenceServiceTestApplication.class, args);
	}
	
	@Bean
    public CommandLineRunner mappingDemo(ParticipantRepository participantRepository,
							            ConferenceRepository conferenceRepository, 
							            ConferenceRoomRepository conferenceRoomRepository
) {
        return args -> {

            // create participants
        	Participant participant1 = new Participant("Voldemar Voodilina", -1564987456);
        	Participant participant2 = new Participant("Patricia Pakiraam", -18686656);
        	Participant participant3 = new Participant("Florian Faabula", 612465344);
        	Participant participant4 = new Participant("Calle Cayacas", 612465344);

            // save participants
        	participantRepository.saveAll(Arrays.asList(participant1, participant2, participant3, participant4));

            // create conference rooms
            ConferenceRoom room1 = new ConferenceRoom("Ruum 1", "Teenindusmaja", 3);
            ConferenceRoom room2 = new ConferenceRoom("Ruum 2", "Postimaja", 3);
            ConferenceRoom room3 = new ConferenceRoom("Ruum 3", "Kaubamaja", 3);
            // save rooms
            conferenceRoomRepository.saveAll(Arrays.asList(room1, room2, room3));
        	
            // create three conferences                                           
            Conference conference1 = new Conference("Conference", 1592113560000L, 1592120880000L, room2, true);
            Conference conference2 = new Conference("Happened Long Time Ago", -143875401000L, -112339401000L, room3);
            Conference conference3 = new Conference("2038", 2147251860000L, 2147330880000L, room1);
            Conference conference4 = new Conference("Vanaaasta", 1609397700000L, 1609451940000L, room2);
            Conference conference5 = new Conference("Uusaasta", 1609484100000L, 1609538340000L, room3);
            Conference conference6 = new Conference("Aastavahetus", 1609311300000L, 1609538340000L, room1);
            Conference conference7 = new Conference("Summer Conference 1 TimeClasher", 1596261300000L, 1596268800000L, room1);
            Conference conference8 = new Conference("Summer Conference 1", 1596261300000L, 1596268800000L, room1);
            Conference conference9 = new Conference("Summer Conference 2", 1596261300000L, 1596268800000L, room2);
            Conference conference10 = new Conference("Summer Conference 3", 1596261300000L, 1596268800000L, room3);
            Conference conference11 = new Conference("Covid-19 9th Wave", 1603702800000L, 1603810800000L, room3);
            Conference conference12 = new Conference("Latest Innovation of Food Microbiology and Advancement in Food Safety Research", 1595685600000L, 1595772000000L, room1);

            conference1.setParticipants(Arrays.asList(participant1));
            conference2.setParticipants(Arrays.asList(participant1, participant2));
            conference3.setParticipants(Arrays.asList(participant1, participant2, participant3));
            conferenceRepository.saveAll(Arrays.asList(conference1, conference2, conference3, conference4, conference5, conference6, conference7, conference8, conference9, conference10, conference11, conference12 ));
            
        };
    }
}
